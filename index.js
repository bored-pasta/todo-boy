class App{
	constructor() {
		this.taskList = document.querySelector("#task-list");
		this.taskPrompt = document.querySelector("#task-prompt");
		this.tasks = this.loadTasks();
		for (const task of this.tasks) {
			this.pushTaskDOM(task);
		}
		this.taskPrompt.addEventListener("keydown", this.handlePrompt.bind(this));
	}

	handlePrompt(event) {
		if (event.key == "Enter" && this.taskPrompt.value != "") {
			const task = this.pushTask(this.taskPrompt.value, false);
			this.saveTasks();
			this.pushTaskDOM(task);
			this.taskPrompt.value = "";
		}
	}

	handleMark(event) {
		const button = event.currentTarget;
		const task = this.tasks[this.getTaskIndex(button.parentElement)];
		task.isDone = !task.isDone;
		this.saveTasks();
		this.setCheckIcon(button, task.isDone);
	}

	handleDelete(event) {
		const button = event.currentTarget;
		const taskIndex = this.getTaskIndex(button.parentElement);
		this.tasks.splice(taskIndex, 1);
		this.saveTasks();
		this.taskList.removeChild(this.taskList.children[taskIndex]);
	}

	setCheckIcon(button, isOn) {
		if (isOn) {
			const icon = this.makeIcon(feather.icons.check.toSvg());
			button.replaceChildren(icon);
		} else {
			button.replaceChildren();
		}
	}
	
	makeIcon(iconStr) {
		const parser = new DOMParser();
		const iconHTML = parser.parseFromString(iconStr, "text/html");
		return iconHTML.body.children[0];
	}

	loadTasks() {
		return JSON.parse(localStorage.getItem("tasks")) || [];
	}

	saveTasks() {
		localStorage.setItem("tasks", JSON.stringify(this.tasks))
	}

	pushTask(text, isDone = false) {
		this.tasks.push({ isDone: isDone, text: text });
		return this.tasks.at(-1);
	}

	pushTaskDOM(task) {
		const taskEntry = document.createElement("div");
		taskEntry.classList.add("task-entry");

		// create check button
		const checkButton = document.createElement("button");
		checkButton.classList.add("flex-center");
		this.setCheckIcon(checkButton, task.isDone);
		checkButton.addEventListener("click", this.handleMark.bind(this));

		// create task text
		const taskText = document.createElement("p");
		taskText.classList.add("task-text");
		taskText.textContent = task.text;

		// create delete button
		const deleteButton = document.createElement("button");
		deleteButton.classList.add("delete-button", "flex-center");
		const deleteIcon = this.makeIcon(feather.icons.x.toSvg());
		deleteButton.appendChild(deleteIcon);
		deleteButton.addEventListener("click", this.handleDelete.bind(this));

		// add elements to entry
		taskEntry.appendChild(checkButton);
		taskEntry.appendChild(taskText);
		taskEntry.appendChild(deleteButton);
		this.taskList.appendChild(taskEntry, this.taskList.firstChild);
	}

	getTaskIndex(task) {
		const tasksDOM = Array.from(this.taskList.children);
		return tasksDOM.indexOf(task);
	}
}

const app = new App()
